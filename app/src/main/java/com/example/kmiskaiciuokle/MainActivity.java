package com.example.kmiskaiciuokle;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.text.DecimalFormat;

public class MainActivity extends AppCompatActivity {
    EditText edit1;
    EditText edit2;
    Button button;
    TextView atsakymas;
    String textFromInput1;
    String textFromInput2;
    float ugis;
    float svoris;
    float kmi;
    String ats;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edit1= findViewById(R.id.edit1);
        edit2= findViewById(R.id.edit2);
        button= findViewById(R.id.button);
        atsakymas= findViewById(R.id.atsakymas);

        final DecimalFormat currency= new DecimalFormat("$###,###.##");


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                textFromInput1=edit1.getText().toString();
                textFromInput2=edit2.getText().toString();
                ugis=Float.parseFloat(textFromInput1);
                svoris=Float.parseFloat(textFromInput2);
                kmi=svoris/(ugis*ugis);
                ats=String.valueOf(kmi);


                Log.d("Skaic", "veikia");
                atsakymas.setText( currency.format(ats));
            }
        });
    }
}
